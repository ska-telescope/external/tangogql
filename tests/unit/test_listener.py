#!/usr/bin/env python3
"""Format methods for converting from PyTango events"""
import numpy as np
from tango import ArgType, CmdArgType, DevFailed, AttrDataFormat
from unittest import mock


def error_str(err):
    """Simple formatter for an error"""
    if isinstance(err, DevFailed):
        return str(err)
    return str(err)


def format_value(value, attr_type):
    """
    Convert from PyTango types to either a String (DevState) or a List if
    it is an array
    """
    if attr_type is ArgType.DevState:
        return str(value)
    if isinstance(value, np.ndarray):
        return value.tolist()
    return value


def format_value_event(evt):
    """
    Format the data from a PyTango change value event
    """
    value = getattr(evt, "value", None)
    w_value = getattr(evt, "w_value", None)
    return {
        "value": format_value(value, evt.type),
        "w_value": format_value(w_value, evt.type),
        "quality": str(evt.quality),
        "time": evt.time.totime(),
    }


def format_config_event(evt):
    """
    Format the data from a PyTango configuration event
    """
    return {
        "description": evt.description,
        "label": evt.label,
        "unit": evt.unit if evt.unit != "No unit" else None,
        "format": evt.format if evt.format != "Not specified" else None,
        "data_format": str(evt.data_format),
        "data_type": str(CmdArgType.values[evt.data_type]),
    }


# Test
def test_config_event_formatter():
    attr = mock.Mock()
    attr.description = "No description"
    attr.label = "long_scalar"
    attr.unit = "F"
    attr.format = "%d"
    attr.data_format = AttrDataFormat.SCALAR
    attr.data_type = CmdArgType.DevLong

    formatted = format_config_event(attr)
    expected = {
        "description": "No description",
        "label": "long_scalar",
        "unit": "F",
        "format": "%d",
        "data_format": str(AttrDataFormat.SCALAR),
        "data_type": str(CmdArgType.values[CmdArgType.DevLong]),
    }

    assert formatted == expected


if __name__ == "__main__":
    test_config_event_formatter()
    print("All tests passed!")
