"""Module containing the Base classes for the Tango Schema."""

import logging.config
import os

import yaml

from tangogql.aioattribute.manager import SubscriptionManager
from tangogql.feature_flags import FeatureFlags
from tangogql.tangodb import CachedDatabase, DeviceProxyCache

db = CachedDatabase(ttl=10)
proxies = DeviceProxyCache()
toggle_flags = FeatureFlags()


def setup_logger(
    default_path="logging.yaml", default_level=logging.DEBUG, env_key="LOG_CFG"
):
    """setup a basic logger as part of initialisation"""
    my_logger = logging.getLogger(__name__)

    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, "rt") as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)
    return my_logger


logger = setup_logger()
subscriptions = SubscriptionManager(
    use_evt=toggle_flags.get_flag("publish_subscribe"),
    polling_interval=toggle_flags.get_value("polling_period"),
    polling_notification_interval=toggle_flags.get_value(
        "polling_notification_interval"
    ),
)
