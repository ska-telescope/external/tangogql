import asyncio
import sys
from unittest.mock import MagicMock, patch

import pytest
from graphene.test import Client
from graphql.execution.executors.asyncio import AsyncioExecutor

from tests.unit import queries
from tests.unit.test_schema_mocks import MockDB, MockDeviceProxyCache

# Mock the base module to avoid side effects
mock_base = MagicMock()
sys.modules["tangogql.schema.base"] = mock_base
mock_base.db = MockDB()
mock_base.proxies = MockDeviceProxyCache()

# Import schema after setting up mocks
with patch.dict("sys.modules", {"tangogql.schema.base": mock_base}):
    from tangogql.schema.tango import tangoschema

# Custom TangoGQL Client for testing


class TangogqlClient(object):
    """Simulated TangoGQL client used for unit tests"""

    def __init__(self):
        self.client = Client(tangoschema, allow_subscriptions=True)

    def execute(self, query):
        """Run the provided query within the event loop"""
        loop = asyncio.get_event_loop()
        r = self.client.execute(query, executor=AsyncioExecutor(loop=loop))
        return r["data"]


@pytest.fixture
def client():
    """
    Client fixture used by unit tests to send queries
    to the TangoGQL code
     - simulating requests from Taranta
    """
    return TangogqlClient()


@pytest.mark.usefixtures("client")
class TestDeviceClass:
    """
    Tests primarily of the ability
    to query the properties of the tango device
    """

    def test_device_resolve_name(self, client):
        mock_response = {"devices": [{"name": "sys/tg_test/1"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_NAME_QUERY)
            assert "devices" in result
            assert "name" in result["devices"][0]
            assert result["devices"][0]["name"] == "sys/tg_test/1"

    def test_device_resolve_single_name(self, client):
        mock_response = {"device": {"name": "sys/tg_test/1"}}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.SINGLE_DEVICE_NAME_QUERY)
            assert "device" in result
            assert "name" in result["device"]
            assert result["device"]["name"] == "sys/tg_test/1"

    def test_class_resolve_devices(self, client):
        mock_response = {
            "classes": [
                {"name": "class_name", "devices": [{"name": "sys/tg_test/1"}]}
            ]
        }
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.CLASS_AND_DEVICE_QUERY)
            assert "classes" in result
            assert "name" in result["classes"][0]
            assert "devices" in result["classes"][0]

    def test_device_resolve_state(self, client):
        mock_response = {"devices": [{"state": "ON"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_STATE_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["state"], str)
            assert len(device["state"]) > 0

    def test_device_resolve_properties(self, client):
        mock_response = {
            "devices": [
                {
                    "properties": [
                        {
                            "name": "property_name",
                            "device": "sys/tg_test/1",
                            "value": "property_value",
                        }
                    ]
                }
            ]
        }
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_PROPERTIES_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["properties"], list)
            property = device["properties"][0]
            assert isinstance(property, dict)
            assert "name" in property
            assert "device" in property
            assert "value" in property

    def test_device_resolve_attributes(self, client):
        mock_response = {
            "devices": [
                {
                    "attributes": [
                        {
                            "name": "ampli",
                            "device": "sys/tg_test/1",
                            "datatype": "float",
                            "format": "SCALAR",
                            "dataformat": "IMAGE",
                            "writable": "READ_WRITE",
                            "label": "Amplitude",
                            "unit": "dB",
                            "description": "Signal amplitude",
                            "displevel": "OPERATOR",
                            "value": 10.5,
                            "writevalue": 5.0,
                            "quality": "VALID",
                            "minvalue": 0.0,
                            "maxvalue": 20.0,
                            "minalarm": 1.0,
                            "maxalarm": 19.0,
                            "timestamp": "2021-01-01T00:00:00Z",
                            "enumLabels": ["label1", "label2"],
                        }
                    ]
                }
            ]
        }
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_ATTRIBUTES_QUERY)
            print(result)  # Log the result
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["attributes"], list)
            attribute = device["attributes"][0]
            assert isinstance(attribute, dict)
            assert "name" in attribute
            assert "device" in attribute
            assert "datatype" in attribute
            assert "format" in attribute
            assert "dataformat" in attribute
            assert "writable" in attribute
            assert "label" in attribute
            assert "unit" in attribute
            assert "description" in attribute
            assert "displevel" in attribute
            assert "value" in attribute
            assert "quality" in attribute
            assert "minvalue" in attribute
            assert "maxvalue" in attribute
            assert "minalarm" in attribute
            assert "maxalarm" in attribute
            assert "timestamp" in attribute
            assert "enumLabels" in attribute
            assert attribute["name"].islower()
            for key, value in attribute.items():
                if key in [
                    "minvalue",
                    "maxvalue",
                    "minalarm",
                    "maxalarm",
                    "timestamp",
                    "enumLabels",
                ]:
                    if value is None:
                        assert value is None
                    else:
                        assert isinstance(value, (int, float, str, list))
                elif key not in ["value", "writevalue"]:
                    assert isinstance(value, str)
                else:
                    assert isinstance(value, (int, float)), (
                        "value is not a number: %s" % key
                    )

    def test_lower_cases(self, client):
        mock_response = {"devices": [{"attributes": [{"name": "ampli"}]}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_ATTRIBUTES_QUERY)
            device = result["devices"][0]
            attribute = device["attributes"][0]
            for key, value in attribute.items():
                if key == "name":
                    assert value.islower()

    def test_device_resolve_commands(self, client):
        mock_response = {
            "devices": [
                {
                    "commands": [
                        {
                            "name": "DevBoolean",
                            "tag": 1,
                            "displevel": "OPERATOR",
                            "intype": "DevBoolean",
                            "intypedesc": "Boolean input",
                            "outtype": "DevBoolean",
                            "outtypedesc": "Boolean output",
                        }
                    ]
                }
            ]
        }
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_COMMANDS_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["commands"], list)
            command = device["commands"][0]
            assert isinstance(command, dict)
            assert "name" in command
            assert "tag" in command
            assert "displevel" in command
            assert "intype" in command
            assert "intypedesc" in command
            assert "outtype" in command
            assert "outtypedesc" in command
            for key, value in command.items():
                if key != "tag":
                    assert isinstance(value, str)
                else:
                    assert isinstance(value, (int, float))

    def test_device_resolve_server(self, client):
        mock_response = {
            "devices": [{"server": {"id": "server_id", "host": "server_host"}}]
        }
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_SERVER_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["server"], dict)
            server = device["server"]
            assert "id" in server
            assert "host" in server
            for _, value in server.items():
                assert isinstance(value, str)

    def test_device_resolve_class(self, client):
        mock_response = {"devices": [{"deviceClass": "TestDeviceClass"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_CLASS_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["deviceClass"], str)

    def test_device_resolve_pid(self, client):
        mock_response = {"devices": [{"pid": 1234}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_PID_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["pid"], int)

    def test_device_resolve_started_date(self, client):
        mock_response = {"devices": [{"startedDate": "2021-01-01T00:00:00Z"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_STARTED_DATE_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["startedDate"], str)

    def test_device_resolve_stopped_date(self, client):
        mock_response = {"devices": [{"stoppedDate": "2021-01-02T00:00:00Z"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DEVICE_STOPPED_DATE_QUERY)
            assert "devices" in result
            device = result["devices"][0]
            assert isinstance(device["stoppedDate"], str)


@pytest.mark.usefixtures("client")
class TestDomainClass:
    """Tests primarily of the ability to resolve domain properties"""

    def test_domain_resolve_name(self, client):
        mock_response = {"domains": [{"name": "domain_name"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DOMAIN_NAME_QUERY)
            assert "domains" in result
            domain = result["domains"][0]
            assert "name" in domain
            for _, value in domain.items():
                assert isinstance(value, str)

    def test_domain_resolve_families(self, client):
        mock_response = {
            "domains": [
                {
                    "families": [
                        {
                            "name": "family_name",
                            "domain": "domain_name",
                            "members": [
                                {
                                    "name": "member1",
                                    "state": "active",
                                    "pid": 1234,
                                    "startedDate": "2021-01-01T00:00:00Z",
                                    "stoppedDate": "2021-01-02T00:00:00Z",
                                    "exported": True,
                                    "domain": "domain_name",
                                    "family": "family_name",
                                }
                            ],
                        }
                    ]
                }
            ]
        }
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.DOMAIN_FAMILIES_QUERY)
            print(result)  # Log the result
            assert "domains" in result
            domain = result["domains"][0]
            assert "families" in domain
            family = domain["families"][0]
            assert "name" in family
            assert "domain" in family
            assert "members" in family
            assert isinstance(family["name"], str)
            assert isinstance(family["domain"], str)
            member = family["members"][0]
            assert "name" in member and isinstance(member["name"], str)
            assert "state" in member and isinstance(member["state"], str)
            assert "pid" in member and isinstance(member["pid"], int)
            assert "startedDate" in member and isinstance(
                member["startedDate"], str
            )
            assert "stoppedDate" in member and isinstance(
                member["stoppedDate"], str
            )
            assert "exported" in member and isinstance(
                member["exported"], bool
            )
            assert "domain" in member and isinstance(member["domain"], str)
            assert "family" in member and isinstance(member["family"], str)


@pytest.mark.usefixtures("client")
class TestMemberClass:
    """Tests primarily of the ability to retrieve member details"""

    def test_member_resolve_name(self, client):
        mock_response = {"members": [{"name": "member1"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_NAME_QUERY)
            print(result)  # Log the result
            assert "members" in result
            member = result["members"][0]
            assert "name" in member
            assert isinstance(member["name"], str)

    def test_member_resolve_state(self, client):
        mock_response = {"members": [{"state": "active"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_STATE_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "state" in member
            assert isinstance(member["state"], str)

    def test_member_resolve_device_class(self, client):
        mock_response = {"members": [{"deviceClass": "TestDeviceClass"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_DEVICE_CLASS_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "deviceClass" in member
            assert isinstance(member["deviceClass"], str)

    def test_member_resolve_pid(self, client):
        mock_response = {"members": [{"pid": 1234}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_PID_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "pid" in member
            assert isinstance(member["pid"], int)

    def test_member_resolve_started_date(self, client):
        mock_response = {"members": [{"startedDate": "2021-01-01T00:00:00Z"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_STARTED_DATE_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "startedDate" in member
            assert isinstance(member["startedDate"], str)

    def test_member_resolve_stopped_date(self, client):
        mock_response = {"members": [{"stoppedDate": "2021-01-02T00:00:00Z"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_STOPPED_DATE_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "stoppedDate" in member
            assert isinstance(member["stoppedDate"], str)

    def test_member_resolve_exported(self, client):
        mock_response = {"members": [{"exported": True}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_EXPORTED_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "exported" in member
            assert isinstance(member["exported"], bool)

    def test_member_resolve_domain(self, client):
        mock_response = {"members": [{"domain": "domain_name"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_DOMAIN_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "domain" in member
            assert isinstance(member["domain"], str)

    def test_member_resolve_family(self, client):
        mock_response = {"members": [{"family": "family_name"}]}
        with patch.object(client, "execute", return_value=mock_response):
            result = client.execute(queries.MEMBER_FAMILY_QUERY)
            assert "members" in result
            member = result["members"][0]
            assert "family" in member
            assert isinstance(member["family"], str)
